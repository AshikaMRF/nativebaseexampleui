import React from 'react';
import {StyleSheet, Text, View, Button, AsyncStorage, ScrollView, TouchableOpacity, StatusBar} from 'react-native';
import {createBottomTabNavigator, createAppContainer} from 'react-navigation';

import {createStackNavigator} from 'react-navigation-stack';

import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Home from './screens/Home.js';
import Main from './screens/Main.js';
import Login from './screens/Login.js';
import Signup from './screens/Signup.js';
import Startup from './screens/Startup.js';
import Trackme from './screens/maps/Trackme.js'

import {theme_color} from './config.js';
//import Settings from './screens/Settings.js'
import Account from './screens/Account.js';

import AccountSignout from './screens/AccountSignout.js';
import AccountSignin from './screens/AccountSignin.js';

import SingleProduct from './screens/products/SingleProduct.js';
import OrderFullDetails from './screens/orders/OrderFullDetails.js';

import MyCartItems from './screens/products/MyCartItems.js';
import CartItems from './screens/products/CartItems.js';

import MyTrips from './screens/products/MyTrips.js';
import ExampleThree from './screens/products/ExampleThree.js';

import {Provider} from 'react-redux';
import store from './store/create_store';
import {getToken} from './store/reducers';
import ProductList from './screens/products/ProductList.js';
import {connect} from 'react-redux';

import ChangeAccountDetails from './screens/account/ChangeAccountDetails.js';
import ChangeDeliveryAddress from './screens/account/ChangeDeliveryAddress.js';
import ChangePassword from './screens/account/ChangePassword.js';


class HelpScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Text> Send a mail to supportdelivery@gmail.com </Text>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});


const HomeStack = createStackNavigator({
        Home: Main,
        ProductList: ProductList,
        SingleProduct: SingleProduct,

    },
);
const OrderStack = createStackNavigator({
        MyTrips: MyTrips,
        SingleOrder: OrderFullDetails,
    },
);

const CartStack = createStackNavigator({
        MyCartItems: MyCartItems,
        CartItems: ExampleThree,

        // Orders : MyOrders


    },
);
const AccountSignoutStack = createStackNavigator({
        Account: AccountSignout,
        Login: Login,
        Signup: Signup,
    },
);

const AccountSigninStack = createStackNavigator({
        Account: AccountSignin,
        ChangeAccountDetails: ChangeAccountDetails,
        ChangeDeliveryAddress: ChangeDeliveryAddress,
        ChangePassword: ChangePassword,

    },
);


const TabNavigator = createMaterialBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: {
                tabBarLabel: 'Home',
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Ionicons style={[{color: theme_color}]} size={25} name={'ios-home'}/>
                    </View>),
                barStyle: {backgroundColor: '#fff'},
            },
        },
        Orders: {
            screen: OrderStack,
            navigationOptions: {
                tabBarLabel: 'My Trips',
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Ionicons style={[{color: theme_color}]} size={25} name={'ios-person'}/>
                    </View>),
                barStyle: {backgroundColor: '#fff'},
            },
        },
        // MyCart: {
        //     screen: CartStack,
        //     navigationOptions: {
        //         tabBarLabel: 'My Cart',
        //         tabBarIcon: ({tintColor}) => (
        //             <View>
        //                 <Ionicons style={[{color: tintColor}]} size={25} name={'ios-cart'}/>
        //             </View>),
        //         barStyle: {backgroundColor: theme_color},
        //     },
        // },
        Account: {
            screen: AccountSigninStack,
            navigationOptions: {
                tabBarLabel: 'Account',
                barStyle: {backgroundColor: '#fff'},
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Ionicons style={[{color: theme_color}]} size={25} name={'ios-person'}/>
                    </View>),
            },
        },
    },
    {
        initialRouteName: 'Home',
        activeColor: theme_color, // color of text
        inactiveColor: '#226557',
        barStyle: {backgroundColor: theme_color},
    },
);

const SignOutTabNavigator = createMaterialBottomTabNavigator(
    {
        Home: {
            screen: Startup,
            navigationOptions: {

                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Ionicons style={[{color: theme_color}]} size={25} name={'ios-home'}/>
                    </View>),
                barStyle: {backgroundColor: '#fff'},
                tabBarVisible: false,
            },
        },
        MyCart: {
            screen: CartStack,
            navigationOptions: {
                tabBarLabel: 'My Trips',
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Ionicons style={[{color: theme_color}]} size={25} name={'ios-cart'}/>
                    </View>),
                barStyle: {backgroundColor: '#fff'},
                tabBarVisible: false,
            },
        },
        Account: {
            screen: AccountSignoutStack,
            navigationOptions: {
                tabBarLabel: 'Account',
                barStyle: {backgroundColor: '#fff'},
                tabBarVisible: false,
                tabBarIcon: ({tintColor}) => (
                    <View>
                        <Ionicons style={[{color: theme_color}]} size={25} name={'ios-person'}/>
                    </View>),
            },
        },
    },
    {
        initialRouteName: 'Home',
        activeColor: theme_color,
        inactiveColor: '#226557',

        tabBarVisible: false,
    },
);


const Navigators = createAppContainer(TabNavigator);
const SignOutNavigators = createAppContainer(SignOutTabNavigator);


class AppRoot extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: null,
        };
    }


    render() {
        const {token} = this.props;


        if (token) {
            return (

                <Navigators/>

            );
        } else {
            return (

                <SignOutNavigators/>

            );

        }


    }


}

const mapStateToProps = state => {
    return {token: state.token};
};

function mapDispatchToProps(dispatch) {
    return {

        //  getToken: (user_data) => dispatch(getToken(user_data)),

    };
}


export default AppRoot = connect(mapStateToProps, mapDispatchToProps)(AppRoot);




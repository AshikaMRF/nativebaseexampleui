import React, {Component} from 'react';

import {Text, View, TouchableOpacity, StyleSheet, StatusBar, ImageBackground} from 'react-native';


import {connect} from 'react-redux';

import {theme_color} from '../config';


class AccountSignout extends React.Component {


    constructor(props) {
        super(props);
    };


    render() {

        return (

            <ImageBackground source={require('../images/GreenBg.jpg')} style={{width: '100%', height: '50%',marginTop:100}}>

                <View>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={theme_color} translucent={true}/>
                    <Text style={{marginTop: 190}}> </Text>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('Login')}>

                        <Text style={styles.text}>Driver Login </Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>


        );


    }
}

const styles = StyleSheet.create({

    MainContainer: {

        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5fcff',
        padding: 11,

    },
    button: {
        alignItems: 'center',
        backgroundColor: theme_color,
        padding: 12,
        width: '80%',
        height: 50,
        marginRight: '10%',
        marginLeft: '10%',
        marginTop: 12,
        fontSize: 20,
        borderRadius: 25,
    },
    text: {

        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },

});

const mapStateToProps = state => {
    return {user_data: state.user_data};
};

function mapDispatchToProps(dispatch) {
    return {
        //  deleteToken: user_data => dispatch(deleteToken(user_data)),

    };
}

export default AccountSignout = connect(mapStateToProps, mapDispatchToProps)(AccountSignout);

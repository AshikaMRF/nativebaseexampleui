import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity, Image, StyleSheet, Button, Share, TextInput} from 'react-native';

import {
    Container, Header, Item, Input, Body, Title, Left, Right, ListItem, Icon,
    Tabs, Tab, TabHeading,
} from 'native-base';

// import { getAuthUser } from '../Common/LocalUser'

import Ionicons from 'react-native-vector-icons/Ionicons';
import Faicon from 'react-native-vector-icons/FontAwesome';
import {api_url, base_url, admin_base_url} from '../../config';
import axios from 'axios';
import {connect} from 'react-redux';
import {addTodo, deleteTodo, updateTodo, setOrders} from '../../store/reducers';
import Modal from 'react-native-modal';
import {Table, TableWrapper, Row} from 'react-native-table-component';

import AwesomeAlert from 'react-native-awesome-alerts';


class OrderFullDetails extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            header: <Header style={styles.headerColor}>
                <Body>
                    <Title> <Text style={styles.logoText}> Single Order View </Text></Title>
                </Body>
            </Header>,
        };
    };

    constructor(props) {
        super(props);
        const {navigation} = this.props;


        const order = navigation.getParam('order', 'NO-USER');

        this.state = {

            quantity: 0,
            tableHead: ['Product', 'Qty'],
            tableHead1: ['Order Id  ', order.order_ref],
            tableHead2: ['Order Status', order.status],
            tableHead3: ['Bill Total ', 'Rs.' + order.fulltotal],


            widthArr: [250, 150],
            widthArr1: [250, 150],
            showAlert: false,

        };
    };


    componentDidMount() {

    }

    showAlert = () => {
        this.setState({
            showAlert: true,
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false,
        });
    };

    cancelOrder = async () => {

        const {navigation} = this.props;

        const order = navigation.getParam('order', 'NO-USER');
        let data = {
            order_id: order.id,
        };

        axios.post(api_url + 'cancelorder', data)
            .then(response => {
                if (response.data.status == 200) {

                    // this.setState({my_orders : response.data.my_orders});
                    this.props.setOrders(response.data.my_orders);
                    // this.props.setOrders(response.data.my_orders)
                    alert('order cancelled successfully');
                    //   this.RemoveAllItem();

                    //  // AsyncStorage.setItem('token', response.data.token);
                    this.props.navigation.navigate('MyOrders');
                } else {
                    alert('could not cancel your order' + response.data);
                    // this.props.navigation.navigate('Login')
                }

            })
            .catch((error) => {
                alert(error);
            });


    };

    render() {


        const {navigation} = this.props;
        const state = this.state;
        const {showAlert} = this.state;


        const order = navigation.getParam('order', 'NO-USER');
        const products = navigation.getParam('products', 'NO-USER');
        const tableData = [];

        // for (let i = 0; i < 5; i += 1) {
        //   const rowData = [];
        //   for (let j = 0; j < 9; j += 1) {
        //     rowData.push(`${i}${j}`);
        //   }
        //   tableData.push(rowData);
        // }
        let c = order.order_items;

        for (let i = 0; i < c.length; i += 1) {
            const rowData = [];
            for (let j = 0; j < 3; j += 1) {
                if (j == 0) {
                    rowData.push(`${c[i]['product_name']}`);

                } else if (j == 1) {
                    rowData.push(`${c[i]['quantity']}`);

                } else if (j == 2) {
                    rowData.push(`${c[i]['price']}`);

                } else {
                    rowData.push(`Delete`);
                }

            }
            tableData.push(rowData);
        }


        return (
            <View>

                <ScrollView horizontal={true}>
                    <View style={{marginLeft: 5}}>

                        <Text style={{marginBottom: 10}}> </Text>

                        <Table borderStyle={{borderColor: 'red'}}>
                            <Row data={state.tableHead1} widthArr={state.widthArr1} style={styles.header}
                                 textStyle={styles.text}/>
                            <Row data={state.tableHead2} widthArr={state.widthArr1} style={styles.header}
                                 textStyle={styles.text}/>
                            <Row data={state.tableHead3} widthArr={state.widthArr1} style={styles.header}
                                 textStyle={styles.text}/>

                        </Table>
                        <AwesomeAlert
                            show={showAlert}
                            showProgress={false}
                            title="Are you sure?"
                            message="Do you want to cancel this order?"
                            closeOnTouchOutside={true}
                            closeOnHardwareBackPress={false}
                            showCancelButton={true}
                            showConfirmButton={true}
                            cancelText="No"
                            confirmText="Yes, cancel it"
                            confirmButtonColor="#DD6B55"
                            onCancelPressed={() => {
                                this.hideAlert();
                            }}
                            onConfirmPressed={() => {
                                this.cancelOrder();
                            }}
                        />
                        <Text style={{marginTop: 20, fontSize: 18}}> </Text>
                        <Table borderStyle={{borderColor: 'red'}}>
                            <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header}
                                 textStyle={styles.text}/>
                        </Table>
                        <ScrollView style={styles.dataWrapper}>
                            <Table borderStyle={{borderColor: '#C1C0B9'}}>
                                {
                                    tableData.map((rowData, index) => (
                                        <Row
                                            key={index}
                                            data={rowData}
                                            widthArr={state.widthArr}
                                            style={[styles.row, index % 2 && {backgroundColor: '#F7F6E7'}]}
                                            textStyle={styles.text}
                                        />
                                    ))
                                }
                            </Table>
                        </ScrollView>


                    </View>

                </ScrollView>

                <ScrollView style={styles.profilescroll}>
                    <View>


                        <View style={styles.titleWrap}>


                            <Text style={styles.title}> </Text>


                        </View>


                        {
                            order.status == 'placed' &&
                            <TouchableOpacity style={styles.clearBtn} onPress={this.showAlert}>
                                <Text style={styles.buttonText}>Cancel this order </Text>
                            </TouchableOpacity>

                        }


                    </View>
                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff'},
    header: {height: 50, backgroundColor: '#ba8cd7'},
    text: {textAlign: 'center', fontWeight: '100'},
    dataWrapper: {marginTop: -1},
    row: {height: 40, backgroundColor: '#E7E6E1'},

    profilescroll: {
        marginBottom: 0,
    },
    button: {
        width: '80%',
        backgroundColor: '#4f83cc',
        marginRight: '10%',
        marginLeft: '10%',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12,
    },

    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    texts: {
        fontSize: 16,
        fontWeight: '700',
        color: '#000',
        textAlign: 'center',
    },

    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 10,
        fontWeight: 'bold',
        color: '#ba8cd7',
    },

    headerColor: {
        backgroundColor: '#fff',
    },
    clearBtn: {
        alignItems: 'center',
        backgroundColor: '#9B870C',
        color: '#ffffff',
        padding: 12,
        width: '80%',
        marginTop: 12,
        marginRight: '10%',
        marginLeft: '10%',
        marginBottom: 10,
        borderRadius: 25,

    },


});

function mapStateToProps(state) {
    return {
        cart_items: state.cart_items,

    };
}

function mapDispatchToProps(dispatch) {
    return {
        addTodo: cart_item => dispatch(addTodo(cart_item)),
        setOrders: my_orders => dispatch(setOrders(my_orders)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(OrderFullDetails);


// export default SingleProduct;

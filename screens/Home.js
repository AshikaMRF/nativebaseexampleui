import React, {Component} from 'react';
import {
    Text, View, TouchableOpacity,
    Image, StyleSheet, ActivityIndicator,
    FlatList, TextInput,
} from 'react-native';

import {Header, Body, Title} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

import axios from 'axios';
import {SearchBar} from 'react-native-elements';


import {api_url, base_url} from '../config';

class Home extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            header: null,
        };
    };

    constructor() {
        super();
        this.state = {
            error: null,
            isLoading: false,
            categories: [],
            search: '',
            token: null,
        };
    };

    updateSearch = search => {
        this.setState({search});

        axios.post(api_url + 'product/search-by-name', {name: search})
            .then(response => {
                this.setState({
                    categories: response.data,
                });
            })
            .catch((error) => {
            });


    };

    getCategoryList() {

        axios.get(api_url + 'products')
            .then(response => {
                this.setState({
                    categories: response.data,
                    isLoading: false,
                });
            })
            .catch((error) => {
            });
    }

    onChangeText(text) {

    }


    componentDidMount() {
        this.getCategoryList();
    }


    render() {
        const {navigation} = this.props;
        const {search} = this.state;

        if (!this.state.token) {


            return (
                <View>
                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('Login')}>

                        <Text style={styles.text}>Login </Text>

                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={() => this.props.navigation.navigate('Signup')}>

                        <Text style={styles.text}>Signup </Text>

                    </TouchableOpacity>

                </View>
            );

        } else {
            return (
                <View>
                    <Header style={styles.headerColor}>
                        <Body>
                            <Title> <Text style={styles.logoText}> All-In-One </Text></Title>
                        </Body>
                    </Header>
                    <SearchBar placeholder="Search Here..." style={{backgroundColor: '#f3f3f3'}}
                               onChangeText={this.updateSearch} value={search}/>
                    <FlatList
                        contentContainerStyle={{paddingBottom: 100}}
                        data={this.state.categories}
                        renderItem={({item, index}) => (
                            <TouchableOpacity
                                style={styles.singleCategoryItem}
                                onPress={() => {
                                    this.props.navigation.navigate(
                                        'SingleProduct', {pageTitle: item.name, category: item},
                                    );
                                }}
                                key={index}>
                                <Image source={{uri: base_url + 'images/' + item.image}}
                                       style={styles.singleCategoryImage}
                                />
                                <Text style={styles.singleCategoryItemTitle}>{item.name}</Text>
                            </TouchableOpacity>
                        )}
                        numColumns={2}
                        keyExtractor={(item, index) => index}
                    />
                </View>
            );

        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        flexDirection: 'row',
        backgroundColor: '#f3f3f3',
        paddingTop: 10,
    },
    logo: {
        flex: 1,
        justifyContent: 'center',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 10,
        fontWeight: 'bold',
        color: '#0148bc',
    },
    singleCategoryItem: {
        width: '50%',
        padding: 15,
    },
    singleCategoryImage: {
        width: '100%',
        height: 120,
        justifyContent: 'center',
    },
    singleCategoryItemTitle: {
        marginTop: 10,
        fontSize: 14,
        textAlign: 'center',
    },

    headerColor: {
        backgroundColor: '#fff',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#0000ff',
        padding: 12,
        width: 280,
        marginTop: 12,
    },
    text: {

        color: '#fff',
    },
});


export default Home;

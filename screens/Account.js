import React, {Component} from 'react';

import {Text, View, TouchableOpacity, StyleSheet, AsyncStorage} from 'react-native';
//import Settings from './Settings.js'
import {
    Container, Header, Item, Input, Body, Title, Left, Right, ListItem, Icon,
    Tabs, Tab, TabHeading,
} from 'native-base';
import {connect} from 'react-redux';

import {deleteToken} from '../store/reducers';

class Account extends Component {


    static navigationOptions = ({navigation}) => {
        return {
            header: <Header style={styles.headerColor}>
                <Body>
                    <Title> <Text style={styles.logoText}> My Account </Text></Title>
                </Body>
            </Header>,
        };
    };


    constructor(props) {
        super(props);
        this.state = {
            token: '',
        };

        //this.logout = this.logout.bind(this);

    };

    checkAuth = async () => {
        try {
            let token = await AsyncStorage.getItem('token');
            // alert(token);

            // let ld = JSON.parse(loginDetails);

            if (token != null || token != '') {
                this.setState({token: token});
                // window.location.refresh()
            }


        } catch (error) {
            alert(error);
        }

        //save data with asyncstorage

    };


    componentDidMount() {
        // this.checkAuth()

    }

    logout = async () => {
        let user_data = {

            user_id: 1,
            token: 'kkgg',
            name: 'ashika',
        };

        AsyncStorage.removeItem('token');
        this.props.deleteToken(user_data);


        this.props.navigation.navigate('Login');
    };


    render() {
        const {user_data} = this.props;

        let token = AsyncStorage.getItem('token');


        if (token) {
            return (
                <View>
                    <Text style={styles.logoText}>Name : </Text>
                    <Text style={styles.logoText}>Email Address : </Text>
                    <Text style={styles.logoText}>Phone Number : </Text>
                    <Text style={styles.logoText}>Address : </Text>

                    <TouchableOpacity
                        style={styles.button}
                        onPress={this.logout}>

                        <Text style={styles.text}>Log out </Text>

                    </TouchableOpacity>


                </View>
            );

        } else {
            return (
                <View>


                    <View>
                        <Text style={{marginTop: 100}}> </Text>

                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.props.navigation.navigate('Login')}>

                            <Text style={styles.text}>Login </Text>

                        </TouchableOpacity>


                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.props.navigation.navigate('Signup')}>

                            <Text style={styles.text}>Signup </Text>

                        </TouchableOpacity>

                    </View>

                </View>
            );


        }


    }
}

const styles = StyleSheet.create({

    MainContainer: {

        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5fcff',
        padding: 11,

    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 22,
        fontWeight: 'bold',
        color: '#aa00aa',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#0000ff',
        padding: 12,
        width: '80%',
        height: 50,
        marginRight: '10%',
        marginLeft: '10%',
        marginTop: 12,
        fontSize: 20,
    },
    headerColor: {
        backgroundColor: '#fff',
    },
    text: {

        color: '#fff',
        fontSize: 20,
        fontWeight: 'bold',
    },

});

const mapStateToProps = state => {
    return {user_data: state.user_data};
};

function mapDispatchToProps(dispatch) {
    return {
        deleteToken: user_data => dispatch(deleteToken(user_data)),

    };
}

export default Account = connect(mapStateToProps, mapDispatchToProps)(Account);

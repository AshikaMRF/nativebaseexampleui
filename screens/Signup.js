import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, AsyncStorage, Keyboard} from 'react-native';

// import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {Header, Body, Title} from 'native-base';
import {api_url, theme_color} from '../config';

export default class Signup extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            header: <Header style={styles.headerColor}>
                <Body>
                    <Title> <Text style={styles.logoText}> Signup </Text></Title>
                </Body>
            </Header>,
        };
    };


    constructor(props) {
        super(props);
        this.state = {
            email: '',
            name: '',
            mobile_number: '',
            password: '',
            password_confirmation: '',
        };
    }


    saveData = async () => {
        const {email, password, name, mobile_number, password_confirmation} = this.state;

        //save data with asyncstorage
        let loginDetails = {
            email: email,
            name: name,
            mobile_number: mobile_number,
            password: password,
            // confirm_password : confirm_password
        };

        if (password == '' || name == '' || mobile_number == '') {

            alert('please fill all ');
        }
            // else if(password !==password_confirmation ){
            //     alert("password and confirm password should be same")
        // }

        else {
            axios.post(api_url + 'register', loginDetails)
                .then(response => {
                    // alert("then")
                    if (response.data.status == 200) {
                        AsyncStorage.setItem('token', response.data.token);
                        alert('Successfully registered');
                        this.props.navigation.navigate('Account');
                    } else {
                        this.props.navigation.navigate('Login');
                    }
                })
                .catch((error) => {
                    alert('error in signup, please try again ');
                });

        }


    };

    showData = async () => {
        let loginDetails = await AsyncStorage.getItem('loginDetails');
        let ld = JSON.parse(loginDetails);
        alert('email: ' + ld.email + ' ' + 'password: ' + ld.password);
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={{marginTop: 15}}> </Text>
                <TextInput style={styles.inputBox}
                           onChangeText={(name) => this.setState({name})}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="Name"
                           placeholderTextColor="#002f6c"
                           selectionColor="#fff"

                />

                <TextInput style={styles.inputBox}
                           onChangeText={(mobile_number) => this.setState({mobile_number})}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="Mobile Number"
                           placeholderTextColor="#002f6c"
                           selectionColor="#fff"
                           keyboardType='numeric'
                           onSubmitEditing={() => this.password.focus()}/>


                <TextInput style={styles.inputBox}
                           onChangeText={(password) => this.setState({password})}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="Password"
                           secureTextEntry={true}
                           placeholderTextColor="#002f6c"
                           ref={(input) => this.password = input}
                />


                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText} onPress={this.saveData}>Submit </Text>
                </TouchableOpacity>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        width: 300,
        backgroundColor: '#eeeeee',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#002f6c',
        marginVertical: 10,
    },
    button: {
        width: 300,
        backgroundColor: theme_color,
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 22,
        fontWeight: 'bold',
        color: theme_color,
        textAlign: 'center',
    },
    headerColor: {
        backgroundColor: '#fff',
    },
});

import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Keyboard,
    ScrollView,
    StatusBar,
    ActivityIndicator,
} from 'react-native';

// import {Actions} from 'react-native-router-flux';
import axios from 'axios';
//import Settings from './Settings.js'
import Home from './Home.js';
import {connect} from 'react-redux';

import {setToken, updateUserSingle} from '../store/reducers';
import {Header, Body, Title} from 'native-base';
import {api_url, theme_color} from '../config';
import store from '../store/create_store';

class Login extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            title: <Text style={styles.logoText}> Login </Text>,
            headerLeft: null,
        };
    };


    constructor(props) {
        super(props);

        this.state = {
            email: '0528542762',
            password: '123456',
            token: null,
            loading: false,
        };
    }


    saveData = async () => {
        this.setState({
            loading: true,
        });
        const {email, password} = this.state;
        let loginDetails = {
            mobile_number : '0528542762',
            password: '123456',

        };
        if (password == '' || email == '') {

            alert('please fill Mobile  Number and Password  ');
        } else {
            axios.post(api_url + 'login', loginDetails)
                .then(response => {
                    //alert('then ')
                    if (response.data.status == 200) {
                        this.setState({
                            loading: false,
                        });
                        alert('Successfully logged in ');


                        this.props.setToken(response.data.token);

                        // this.props.setOrders(response.data.orders);
                        // this.props.setAddress(response.data.address);
                        this.props.updateUserSingle(
                            {
                                mobile_number: '0528542762',
                                name: 'Test Driver ',
                                email: 'test@gmail.com',

                            });
                        //               this.props.updateUserSingle(
                        //     {
                        //       mobile_number : response.data.user.mobile_number,
                        //       name :  response.data.user.name,
                        //       email : response.data.user.email,

                        // })


                        this.props.navigation.navigate('Account');

                    } else {

                        alert('Error occured, please try again');

                    }
                })
                .catch((error) => {
                    alert(error);
                });


        }


    };

    render() {
        const {user_data} = this.props;

        if (this.state.loading) {
            return (
                <View style={styles.container}>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={theme_color} translucent={true}/>
                    <View style={{marginTop: 200}}>
                        <ActivityIndicator/>
                    </View>

                </View>
            );

        } else {
            return (
                <View style={styles.container}>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={theme_color} translucent={true}/>
                    <Text style={{marginTop: 15}}> </Text>
                    <TextInput style={styles.inputBox}
                               onChangeText={(email) => this.setState({email})}
                               underlineColorAndroid='rgba(0,0,0,0)'
                               placeholder="Mobile Number"
                               placeholderTextColor="#002f6c"
                               selectionColor="#fff"
                               keyboardType='numeric'
                               onSubmitEditing={() => this.password.focus()}/>


                    <TextInput style={styles.inputBox}
                               onChangeText={(password) => this.setState({password})}
                               underlineColorAndroid='rgba(0,0,0,0)'
                               placeholder="Password"
                               secureTextEntry={true}
                               placeholderTextColor="#002f6c"
                               ref={(input) => this.password = input}
                    />

                    <TouchableOpacity activeOpacity={0.5} style={styles.button}>
                        <Text style={styles.buttonText} onPress={this.saveData}>Submit </Text>
                    </TouchableOpacity>

                    <ScrollView>

                    </ScrollView>

                </View>
            );

        }


    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        width: 300,
        backgroundColor: '#eeeeee',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#002f6c',
        marginVertical: 10,
    },
    button: {
        width: 300,
        backgroundColor: theme_color,
        opacity: 1,
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 42,
        fontWeight: 'bold',
        color: theme_color,
    },
    headerColor: {
        backgroundColor: '#fff',
    },
});

const mapStateToProps = state => {
    return {token: state.token};
};

function mapDispatchToProps(dispatch) {
    return {
        setToken: token => dispatch(setToken(token)),
        //  setOrders : my_orders =>dispatch(setOrders(my_orders)),
        // setAddress : address =>dispatch(setAddress(address)),
        updateUserSingle: user => dispatch(updateUserSingle(user)),

    };
}

export default Login = connect(mapStateToProps, mapDispatchToProps)(Login);



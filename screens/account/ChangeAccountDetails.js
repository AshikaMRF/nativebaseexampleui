import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    AsyncStorage,
    Keyboard,
    ScrollView,
    Input,
} from 'react-native';

// import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {connect} from 'react-redux';

import {setToken, updateUser, user_data, updateUserSingle} from '../../store/reducers';
import {api_url} from '../../config';
import {Header, Body, Title} from 'native-base';

class ChangeAccountDetails extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            header: <Header style={styles.headerColor}>
                <Body>
                    <Title> <Text style={styles.logoText}> Change Account Details </Text></Title>
                </Body>
            </Header>,
        };
    };


    constructor(props) {
        super(props);
        const {user_data} = this.props;

        this.state = {
            email: user_data[0].email,
            name: user_data[0].name,
            mobile_number: user_data[0].user.mobile_number,
        };
    }


    componentDidMount() {

    }

    cancel = () => {
        this.props.navigation.navigate('Account');
    };


    saveData = async () => {
        const {email, name, mobile_number} = this.state;
        const {user_data} = this.props;

        let data = {
            email: email,
            name: name,
            mobile_number: mobile_number,
            user_id: user_data[0].user_id,

        };
        if (email == '' || name == '' || mobile_number == '') {
            alert('please fill all details');
        } else {
            axios.post(api_url + 'change-account-details', data)
                .then(response => {
                    //alert('then ')
                    if (response.data.status == 200) {

                        let user_data = {


                            name: response.data.user.name,
                            email: response.data.user.email,
                            mobile_number: response.data.user.mobile_number,

                        };
                        this.props.updateUser(user_data);
                        this.props.updateUserSingle({


                            name: response.data.user.name,
                            email: response.data.user.email,
                            mobile_number: response.data.user.mobile_number,
                        });

                        alert('success');
                        this.props.navigation.navigate('Account');
                    } else {

                        alert('Error occured, please try again');
                        // this.props.navigation.navigate('Login')
                    }
                })
                .catch((error) => {
                    alert(error);
                });

        }

    };

    render() {
        const {user_data} = this.props;

        return (
            <View style={styles.container}>
                <Text> </Text>

                <TextInput style={styles.inputBox} value={this.state.name}
                           onChangeText={(name) => this.setState({name})}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="Name"

                           placeholderTextColor="#002f6c"

                />

                <TextInput style={styles.inputBox} value={this.state.mobile_number}
                           onChangeText={(mobile_number) => this.setState({mobile_number})}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="Mobile Number "

                           placeholderTextColor="#002f6c"

                />

                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText} onPress={this.saveData}>Save </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText} onPress={this.cancel}>Cancel </Text>
                </TouchableOpacity>


                <ScrollView>

                </ScrollView>

            </View>
        );


    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerColor: {
        backgroundColor: '#fff',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 22,
        fontWeight: 'bold',
        color: '#ba8cd7',
    },
    inputBox: {
        width: 300,
        backgroundColor: '#eeeeee',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#002f6c',
        marginVertical: 10,
    },
    button: {
        width: 300,
        backgroundColor: '#ba8cd7',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
});

const mapStateToProps = state => {
    return {user_data: state.user_data};
};

function mapDispatchToProps(dispatch) {
    return {
        setToken: user_data => dispatch(setToken(user_data)),
        updateUser: user_data => dispatch(updateUser(user_data)),
        updateUserSingle: user => dispatch(updateUserSingle(user)),


    };
}

export default ChangeAccountDetails = connect(mapStateToProps, mapDispatchToProps)(ChangeAccountDetails);



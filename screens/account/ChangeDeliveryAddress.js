import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity, AsyncStorage, Keyboard, ScrollView} from 'react-native';

// import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import {connect} from 'react-redux';

import {setToken, setAddress} from '../../store/reducers';
import {api_url} from '../../config';
import {Header, Body, Title} from 'native-base';


class ChangeAccountDetails extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            header: <Header style={styles.headerColor}>
                <Body>
                    <Title> <Text style={styles.logoText}> Change Delivery Address </Text></Title>
                </Body>
            </Header>,
        };
    };


    constructor(props) {
        super(props);
        const {user_data, address} = this.props;

        this.state = {
            house_no: address ? address.house_no : '',
            street_name: address ? address.street_name : '',
            area_name: address ? address.area_name : '',
        };
    }


    componentDidMount() {

    }

    cancel = () => {
        this.props.navigation.navigate('Account');
    };


    saveData = async () => {
        const {house_no, street_name, area_name} = this.state;
        const {user_data, setAddress} = this.props;

        let loginDetails = {
            house_no: house_no,
            street_name: street_name,
            area_name: area_name,
            // user_id : AsyncStorage.getItem('user_id'),
            user_id: user_data[0].user_id,

        };
        if (house_no == '' || street_name == '' || area_name == '') {
            alert('please fill all details');
        } else {
            axios.post(api_url + 'change-delivery-address', loginDetails)
                .then(response => {
                    //alert('then ')
                    if (response.data.status == 200) {
                        AsyncStorage.setItem('address', 'true');

                        setAddress(loginDetails);
                        alert('Successfully updated address');


                        // let user_data ={}
                        //  this.props.setToken(user_data);
                        this.props.navigation.navigate('Account');
                    } else {

                        alert('Error occured, please try again');
                        // this.props.navigation.navigate('Login')
                    }
                })
                .catch((error) => {
                    alert(error);
                });

        }

        console.log(loginDetails);


    };


    render() {
        const {user_data} = this.props;


        return (
            <View style={styles.container}>
                <Text> </Text>
                <TextInput style={styles.inputBox} value={this.state.house_no}
                           onChangeText={(house_no) => this.setState({house_no})}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="House No"
                           placeholderTextColor="#002f6c"
                           selectionColor="#fff"
                           onSubmitEditing={() => this.street_name.focus()}/>

                <TextInput style={styles.inputBox}
                           onChangeText={(street_name) => this.setState({street_name})} value={this.state.street_name}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="Street Name"
                           placeholderTextColor="#002f6c"

                />
                <TextInput style={styles.inputBox}
                           onChangeText={(area_name) => this.setState({area_name})} value={this.state.area_name}
                           underlineColorAndroid='rgba(0,0,0,0)'
                           placeholder="Area Name"
                           placeholderTextColor="#002f6c"

                />

                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText} onPress={this.saveData}>Save </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.buttonText} onPress={this.cancel}>Cancel </Text>
                </TouchableOpacity>

            </View>
        );


    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        width: 300,
        backgroundColor: '#eeeeee',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#002f6c',
        marginVertical: 10,
    },
    button: {
        width: 300,
        backgroundColor: '#ba8cd7',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    headerColor: {
        backgroundColor: '#fff',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 22,
        fontWeight: 'bold',
        color: '#ba8cd7',
    },
});

const mapStateToProps = state => {
    return {
        user_data: state.user_data, address: state.address,
    };
};

function mapDispatchToProps(dispatch) {
    return {
        setToken: user_data => dispatch(setToken(user_data)),
        setAddress: address => dispatch(setAddress(address)),

    };
}

export default ChangeDeliveryAddress = connect(mapStateToProps, mapDispatchToProps)(ChangeAccountDetails);



import React, {Component} from 'react';
import {
    Text, View, TouchableOpacity,
    Image, StyleSheet, ActivityIndicator,
    FlatList, TextInput, StatusBar, ScrollView, Button,
} from 'react-native';

import {
    Header,
    Body,
    Title,
    Container,
    Content,
    Card,
    CardItem,
    Left,
    Right,
    Thumbnail,
    Spinner,
    Icon,
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

import axios from 'axios';
import {SearchBar} from 'react-native-elements';
// import { Head } from './components/Head.js';

import {api_url, base_url, theme_color} from '../config';

class Main extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            title: <Text style={styles.logoText}> Rent Vehicles </Text>,
            headerLeft: null,
        };
    };

    constructor() {
        super();
        this.state = {
            error: null,
            isLoading: true,
            categories: [],
            search: '',
            token: null,
        };
    };

    getCategoryList() {

        axios.get(api_url + 'categories')
            .then(response => {
                this.setState({
                    categories: response.data,
                    isLoading: false,
                });
            })
            .catch((error) => {
            });
    }


    componentDidMount() {
        this.getCategoryList();
    }


    render() {
        const {navigation} = this.props;
        const {search} = this.state;

        if (this.state.categories.length > 0) {


            return (
                <View>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={theme_color} translucent={true}/>


                    <View>


                        <ScrollView horizontal={true}>

                            <View style={{marginLeft: 5, width: 350}}>
                                <ScrollView style={styles.dataWrapper}>

                                    {
                                        this.state.categories.map((rowData, index) => (
                                            <View>
                                                <Card>
                                                    <CardItem>
                                                        <Left style={{flex: 0.5}}>
                                                            <Image source={{uri: base_url + 'images/' + rowData.image}}
                                                                   style={{width: 150, height: 100}}/>
                                                        </Left>
                                                        <View style={{flex: 0.5}}>

                                                            <Text style={{
                                                                textAlign: 'left',
                                                                marginLeft: 20,
                                                                fontSize: 19,
                                                            }}> {rowData.name}   </Text>
                                                        </View>

                                                    </CardItem>
                                                </Card>
                                            </View>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        </ScrollView>
                    </View>


                </View>
            );
        } else {
            return (
                <View>
                    <View style={{marginTop: 200}}>
                        <ActivityIndicator/>
                    </View>

                </View>
            );
        }


    }
}

const styles = StyleSheet.create({

    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
        top: 200,
    },
    header: {
        flexDirection: 'row',
        backgroundColor: '#f3f3f3',
        paddingTop: 10,
    },

    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 10,
        fontWeight: 'bold',
        color: theme_color,
    },
    singleCategoryItem: {
        width: '98%',
        padding: 15,
        height: 100,
        // backgroundColor : '#ba8cd7',
        color: '#fff',
        margin: 10,
        fontSize: 30,
        fontWeight: 'bold',
        borderRadius: 20,
        borderWidth: 2,
        borderColor: theme_color,
        marginRight: 7,
    },
    singleCategoryImage: {
        width: '100%',
        height: 120,
        justifyContent: 'center',
    },
    singleCategoryItemTitle: {
        marginTop: 10,
        fontSize: 25,
        textAlign: 'center',
    },

    headerColor: {
        backgroundColor: '#fff',
    },
    text: {

        color: '#fff',
    },
});


export default Main;

import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, AsyncStorage, StatusBar} from 'react-native';
import {
    Container, Header, Item, Input, Body, Title, Left, Right, ListItem, Icon,
    Tabs, Tab, TabHeading,
} from 'native-base';
import {connect} from 'react-redux';

import {deleteToken} from '../store/reducers';
import {api_url, base_url, theme_color} from '../config';

class AccountSignin extends Component {


    static navigationOptions = ({navigation}) => {
        return {
            title: <Text style={styles.logoText}> My Account </Text>,
            headerLeft: null,
        };
    };

    constructor(props) {
        super(props);
        this.state = {
            token: '',
        };


    };

    logout = async () => {

        this.props.deleteToken('');


        //this.props.navigation.navigate('Login')
    };


    render() {
        const {user_data, address, user} = this.props;

        let name = AsyncStorage.getItem('name');


        return (
            <View>
                <StatusBar barStyle="dark-content" hidden={false} backgroundColor={theme_color} translucent={true}/>

                <Text style={styles.accountText}>Name : {user.name} </Text>

                <Text style={styles.accountText}>Phone Number : {user.mobile_number} </Text>


                <Text style={styles.accountText}> </Text>
                <TouchableOpacity
                    style={styles.changebutton}
                    onPress={() => {
                        this.props.navigation.navigate(
                            '', {pageTitle: 'l'},
                        );
                    }}>

                    <Text style={styles.text}>Change Account Details </Text>

                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.changebutton}
                    onPress={() => {
                        this.props.navigation.navigate(
                            '', {pageTitle: 'l'},
                        );
                    }}>

                    <Text style={styles.text}>Change Vehicle Details </Text>

                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.changebutton}
                    onPress={() => {
                        this.props.navigation.navigate(
                            '', {pageTitle: 'l'},
                        );
                    }}>

                    <Text style={styles.text}>Change Password </Text>

                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.button}
                    onPress={this.logout}>

                    <Text style={styles.text}>Log out </Text>

                </TouchableOpacity>


            </View>
        );


    }
}

const styles = StyleSheet.create({

    MainContainer: {

        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5fcff',
        padding: 11,

    },

    accountText: {
        fontSize: 18,
        marginTop: 22,
        color: '#000',
        marginLeft: 10,
    },
    button: {
        alignItems: 'center',
        backgroundColor: theme_color,
        marginTop: 20,
        borderRadius: 25,
        padding: 9,
        width: '80%',
        height: 40,
        marginRight: '10%',
        marginLeft: '10%',
        marginTop: 12,
        fontSize: 15,
    },
    changebutton: {
        alignItems: 'center',
        backgroundColor: theme_color,
        marginTop: 20,
        borderRadius: 25,
        padding: 9,
        width: '80%',
        height: 40,
        marginRight: '10%',
        marginLeft: '10%',
        marginTop: 12,
        fontSize: 15,

    },
    headerColor: {
        backgroundColor: '#fff',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 22,
        fontWeight: 'bold',
        color: theme_color,
    },
    text: {

        color: '#fff',
        fontSize: 16,

    },

});

const mapStateToProps = state => {
    return {user_data: state.user_data, address: state.address, user: state.user};
};

function mapDispatchToProps(dispatch) {
    return {
        deleteToken: user_data => dispatch(deleteToken(user_data)),

    };
}

export default Account = connect(mapStateToProps, mapDispatchToProps)(AccountSignin);

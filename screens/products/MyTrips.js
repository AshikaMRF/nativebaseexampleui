import React, {Component} from 'react';
import {
    Text, View, TouchableOpacity,
    Image, StyleSheet, ActivityIndicator,
    FlatList, TextInput, StatusBar,
} from 'react-native';

import {Header, Body, Title} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';

import axios from 'axios';
import {SearchBar} from 'react-native-elements';


import {api_url, base_url, theme_color} from '../../config';
import {connect} from 'react-redux';


class MyTrips extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            title: <Text style={styles.logoText}> Recent Trips </Text>,
            headerLeft: null,
        };
    };

    constructor() {
        super();
        this.state = {
            error: null,
            isLoading: false,
            categories: [],
            search: '',
            token: null,
        };
    };


    render() {
        const {navigation, my_orders} = this.props;
        const {search} = this.state;


        if (my_orders.length > 0) {


            return (
                <View>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={theme_color} translucent={true}/>


                    <View>


                        <FlatList
                            contentContainerStyle={{paddingBottom: 100, padding: 5}}
                            data={my_orders}
                            renderItem={({item, index}) => (
                                <TouchableOpacity
                                    style={styles.singleCategoryItem}
                                    onPress={() => {
                                        this.props.navigation.navigate(
                                            'SingleOrder', {pageTitle: item.name, products: item.products, order: item},
                                        );
                                    }}
                                    key={index}>

                                    <Text style={styles.singleCategoryItemTitle}> OrderID {item.order_ref}</Text>

                                    {item.status == 'placed' &&
                                    <Text style={{color: 'green'}}> Status - {item.status}</Text>}
                                    {item.status == 'cancelled' &&
                                    <Text style={{color: 'red'}}> Status - {item.status}</Text>}


                                </TouchableOpacity>
                            )}
                            numColumns={1}
                            keyExtractor={(item, index) => index}
                        />
                    </View>

                </View>
            );

        } else {
            return (
                <View>
                    <StatusBar barStyle="dark-content" hidden={false} backgroundColor={theme_color} translucent={true}/>


                    <Text style={styles.nocartItems}> You have no trips yet ! </Text>

                </View>
            );

        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    nocartItems: {
        color: 'red',
        fontWeight: '800',
        fontSize: 25,
        marginTop: 150,
        marginLeft: 50,
        marginBottom: 150,
        justifyContent: 'center',
    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        flexDirection: 'row',
        backgroundColor: '#f3f3f3',
        paddingTop: 10,
    },
    logo: {
        flex: 1,
        justifyContent: 'center',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 10,
        fontWeight: 'bold',
        color: theme_color,
    },
    singleCategoryItem: {
        width: '94%',
        padding: 15,
        height: 100,
        borderBottomWidth: 1,
        color: '#fff',
        margin: 10,
        fontSize: 30,
        fontWeight: 'bold',
        borderColor: '#aa00aa',

    },
    singleCategoryImage: {
        width: '100%',
        height: 120,
        justifyContent: 'center',
    },
    singleCategoryItemTitle: {
        marginTop: 10,
        fontSize: 25,
        textAlign: 'center',
    },

    headerColor: {
        backgroundColor: '#fff',
    },
    button: {
        alignItems: 'center',
        backgroundColor: '#0000ff',
        padding: 12,
        width: 280,
        marginTop: 12,
    },
    text: {

        color: '#fff',
    },
});


const mapStateToProps = state => {
    return {
        cart_items: state.cart_items,
        user_data: state.user_data,
        my_orders: state.my_orders,
    };
};

function mapDispatchToProps(dispatch) {
    return {
        setOrders: (my_orders) => dispatch(setOrders(my_orders)),
    };
}

export default MyTrips = connect(mapStateToProps, mapDispatchToProps)(MyTrips);



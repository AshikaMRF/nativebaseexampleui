import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, TouchableOpacity, Text} from 'react-native';
import {Table, TableWrapper, Row} from 'react-native-table-component';
import {connect} from 'react-redux';

class ExampleThree extends Component {
    constructor(props) {
        super(props);
        const {cart_total} = this.props;
        this.state = {

            tableHead: ['Product', 'Qty', 'Price'],
            tableHead1: ['Cart Total ', 'Rs.' + cart_total],
            tableHead2: ['Delivery Fee', 'Rs.25'],
            tableHead3: ['Bill Total ', 'Rs.' + (cart_total + 25)],


            widthArr: [150, 100, 150],
            widthArr1: [250, 150],
        };
    }

    render() {
        const state = this.state;

        const {cart_items} = this.props;
        const {navigation} = this.props;

        const order = navigation.getParam('order', 'NO-USER');

        const tableData = [];

        // for (let i = 0; i < 5; i += 1) {
        //   const rowData = [];
        //   for (let j = 0; j < 9; j += 1) {
        //     rowData.push(`${i}${j}`);
        //   }
        //   tableData.push(rowData);
        // }
        let c = cart_items;

        for (let i = 0; i < c.length; i += 1) {
            const rowData = [];
            for (let j = 0; j < 3; j += 1) {
                if (j == 0) {
                    rowData.push(`${c[i]['product_name']}`);

                } else if (j == 1) {
                    rowData.push(`${c[i]['quantity']}`);

                } else if (j == 2) {
                    rowData.push(`${c[i]['price']}`);

                } else {
                    rowData.push(`Delete`);
                }

            }
            tableData.push(rowData);
        }


        return (
            <View>
                <ScrollView horizontal={true}>
                    <View style={{marginLeft: 5}}>
                        <Table borderStyle={{borderColor: 'red'}}>
                            <Row data={state.tableHead} widthArr={state.widthArr} style={styles.header}
                                 textStyle={styles.text}/>
                        </Table>
                        <TouchableOpacity style={styles.clearBtn} onPress={this.cancelOrder}>
                            <Text style={styles.buttonText}>Cancel this order </Text>
                        </TouchableOpacity>


                        <ScrollView style={styles.dataWrapper}>
                            <Table borderStyle={{borderColor: '#C1C0B9'}}>
                                {
                                    tableData.map((rowData, index) => (
                                        <Row
                                            key={index}
                                            data={rowData}
                                            widthArr={state.widthArr}
                                            style={[styles.row, index % 2 && {backgroundColor: '#F7F6E7'}]}
                                            textStyle={styles.text}
                                        />
                                    ))
                                }
                            </Table>
                        </ScrollView>
                        <Text style={{marginBottom: 10}}> </Text>

                        <Table borderStyle={{borderColor: 'red'}}>
                            <Row data={state.tableHead1} widthArr={state.widthArr1} style={styles.header}
                                 textStyle={styles.text}/>
                            <Row data={state.tableHead2} widthArr={state.widthArr1} style={styles.header}
                                 textStyle={styles.text}/>
                            <Row data={state.tableHead3} widthArr={state.widthArr1} style={styles.header}
                                 textStyle={styles.text}/>

                        </Table>

                    </View>
                </ScrollView>

                <Text style={{color: 'red '}}> Total : Rs. {order.fulltotal}  </Text>


            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff'},
    header: {height: 50, backgroundColor: '#ba8cd7'},
    text: {textAlign: 'center', fontWeight: '100'},
    dataWrapper: {marginTop: -1},
    row: {height: 40, backgroundColor: '#E7E6E1'},
});

const mapStateToProps = state => {
    return {
        cart_items: state.cart_items,
        cart_total: state.cart_total,

    };
};


export default CartItems = connect(mapStateToProps)(ExampleThree);



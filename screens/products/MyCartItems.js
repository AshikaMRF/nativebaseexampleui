import React, {Component} from 'react';
import {connect} from 'react-redux';

import {deleteTodo, emptyStore, getTotal, setOrders} from '../../store/reducers';
import {
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
    AsyncStorage,
    Input,
    Form,
    TextInput,
    FlatList,
    ScrollView,
} from 'react-native';

import {Header, Body, Title} from 'native-base';
import axios from 'axios';
import {api_url, base_url, admin_base_url} from '../../config';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ExampleThree from './ExampleThree.js';
import AwesomeAlert from 'react-native-awesome-alerts';

class ConnectedForm extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            header: <Header style={styles.headerColor}>
                <Body>
                    <Title> <Text style={styles.logoText}> My Cart </Text></Title>
                </Body>
            </Header>,
        };
    };


    constructor(props) {
        super(props);
        const {my_orders} = this.props;

        this.state = {
            title: '',
            cart_total: 0,
            my_orders: my_orders,
            showAlert: false,

        };
        // alert(props.cart_items)
        // this.handleChange = this.handleChange.bind(this);
        // this.handleSubmit = this.handleSubmit.bind(this);
    }

    viewCartItems = () => {
        this.props.navigation.navigate('CartItems');
    };
    placeOrder = async () => {
        const {cart_items, cart_total, user_data} = this.props;

        let cartDetails = {
            cart_items: cart_items,
            user_id: user_data[0].user_id,
            fulltotal: cart_total + 25,
        };
        console.log(cartDetails);
        this.setState({showAlert: false});

        axios.post(api_url + 'placeorder', cartDetails)
            .then(response => {
                if (response.data.status == 200) {

                    this.setState({my_orders: response.data.my_orders});

                    this.props.setOrders(response.data.my_orders);
                    alert('order placed successfully');
                    this.RemoveAllItem();

                    // AsyncStorage.setItem('token', response.data.token);
                    this.props.navigation.navigate('MyOrders');
                } else {
                    alert('please provide correct details');
                    // this.props.navigation.navigate('Login')
                }

            })
            .catch((error) => {
                alert(error);
            });

    };
    RemoveAllItem = () => {
        this.props.emptyStore();
        //this.props.getTotal()

    };
    findTotal = () => {
        const {cart_items} = this.props;
        let cart_total = 0;
        cart_items.map((item, key) => (

            cart_total = item.total + cart_total

        ));
        this.setState({cart_total: total});
        return cart_total;

    };
    showAlert = () => {
        this.setState({
            showAlert: true,
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false,
        });
    };


    render() {
        const {title} = this.state;
        const {cart_items, user_data, cart_total} = this.props;
        const {showAlert} = this.state;
        const {emptyStore, deleteTodo} = this.props;
        let token = AsyncStorage.getItem('token');
        const {navigation} = this.props;


        if (cart_items.length > 0) {
            return (
                <View>


                    {
                        user_data.length > 0 && <TouchableOpacity style={styles.placeorderBtn} onPress={this.showAlert}>
                            <Text style={styles.buttonText}>Place Order </Text>
                        </TouchableOpacity>

                    }
                    <AwesomeAlert
                        show={showAlert}
                        showProgress={false}
                        title="Are you sure? "
                        message="Do You want to place this order?"
                        closeOnTouchOutside={true}
                        closeOnHardwareBackPress={false}
                        showCancelButton={true}
                        showConfirmButton={true}
                        cancelText="No, cancel"
                        confirmText="Yes, Confirm "
                        confirmButtonColor="#DD6B55"
                        onCancelPressed={() => {
                            this.hideAlert();
                        }}
                        onConfirmPressed={() => {
                            this.placeOrder();
                        }}
                    />


                    {!showAlert && <ExampleThree/>}
                    <Text style={[{color: '#ff0000', alignSelf: 'flex-end'}]}> </Text>


                    <Text style={{marginBottom: 150}}> </Text>


                    <View>

                    </View>

                </View>
            );
        } else {
            return (
                <View>
                    <Text style={styles.nocartItems}> Your cart is empty ! </Text>
                </View>
            );

        }


    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemText: {
        color: '#9B870C',
        fontSize: 29,
        marginLeft: 15,
    },


    textStyle: {
        fontSize: 20,
    },
    productName: {
        fontSize: 25,
        color: '#ba8cd7',
    },
    cartText: {
        fontSize: 27,
        color: '#ba8cd7',
        marginLeft: 20,
    },

    nocartItems: {
        color: 'red',
        fontWeight: '800',
        fontSize: 25,
        marginTop: 150,
        marginLeft: 50,
        marginBottom: 150,
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    removebuttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ff0000',
        textAlign: 'center',
    },

    button: {
        alignItems: 'center',
        padding: 12,
        width: 280,
        marginTop: 12,
        marginBottom: 10,

    },
    cartItems: {
        color: '#ffffff',
        fontSize: 20,
        padding: 5,
        width: '100%',
        marginLeft: '10%',
        marginTop: 5,
        marginBottom: 5,

    },


    placeorderBtn: {
        alignItems: 'center',
        backgroundColor: '#ba8cd7',
        color: '#ffffff',
        padding: 12,
        width: '80%',
        marginTop: 12,
        marginRight: '10%',
        marginLeft: '10%',
        marginBottom: 10,
        borderRadius: 25,

    },
    activityIndicator: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        flexDirection: 'row',
        backgroundColor: '#f3f3f3',
        paddingTop: 10,
    },
    logo: {
        flex: 1,
        justifyContent: 'center',
    },
    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 10,
        fontWeight: 'bold',
        color: '#ba8cd7',
    },

    headerColor: {
        backgroundColor: '#fff',
    },

});
const mapStateToProps = state => {
    return {
        cart_items: state.cart_items,
        user_data: state.user_data,
        cart_total: state.cart_total,
        my_orders: state.my_orders,
    };
};

function mapDispatchToProps(dispatch) {
    return {
        deleteTodo: cart_item => dispatch(deleteTodo(cart_item)),
        emptyStore: cart_item => dispatch(emptyStore(cart_item)),
        getToken: (user_data) => dispatch(getToken(user_data)),
        getTotal: () => dispatch(getTotal()),
        setOrders: (my_orders) => dispatch(setOrders(my_orders)),
    };
}

const MyCartItems = connect(mapStateToProps, mapDispatchToProps)(ConnectedForm);

export default MyCartItems;

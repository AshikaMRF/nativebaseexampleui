import React, {Component} from 'react';
import {
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Image,
    StyleSheet,
    Button,
    Dimensions,
    Share,
    TextInput,
} from 'react-native';

import {
    Container, Header, Item, Input, Body, Title, Left, Right, ListItem, Icon,
    Tabs, Tab, TabHeading,
} from 'native-base';

// import { getAuthUser } from '../Common/LocalUser'

import Ionicons from 'react-native-vector-icons/Ionicons';
import Faicon from 'react-native-vector-icons/FontAwesome';
import {api_url, base_url, admin_base_url} from '../../config';
import axios from 'axios';
import {connect} from 'react-redux';
import {addTodo, deleteTodo, updateTodo} from '../../store/reducers';
import Modal from 'react-native-modal';


class SingleProduct extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            header: <Header style={styles.headerColor}>
                <Body>
                    <Title> <Text style={styles.logoText}> Single Product View </Text></Title>
                </Body>
            </Header>,
        };
    };

    constructor(props) {
        super(props);
        const {navigation} = this.props;
        this.state = {
            product: navigation.getParam('category', null),
            isModalVisible: false,
            quantity: '1',
        };
    };

    saveToDoData = (todo) => {
        //this.addNewToDo(show = false);
        this.props.addTodo(todo);
    };
    onChanged = (quantity) => {
        this.setState({quantity: quantity});

    };

    showQuantityArea = () => {
        this.setState({isModalVisible: true});
    };
    hideQuantityArea = () => {
        this.setState({isModalVisible: false});
        this.props.navigation.navigate('Home');
    };


    componentDidMount() {

    }

    addToCart = async () => {

        const {email, password, name, mobile_number, confirm_password, quantity} = this.state;
        const product = this.state.product;

        let cartDetails = {
            product_id: product.id,
            product_name: product.name,
            product_image: product.image,
            price: product.price,
            total: product.price * quantity,
            quantity: quantity,

        };
        if (quantity == 0 || quantity == '') {
            alert('Choose minimum quantity 1');
        } else {
            this.props.addTodo(cartDetails);
            alert('Successfully added to cart');
            this.props.navigation.navigate('Home');

        }


    };

    render() {

        const {new_todo} = this.state;
        const deviceHeight = Dimensions.get('window').height / 4;

        const {navigation, todos, show_new_todo, screen, deleteTodo, updateTodo} = this.props;

        const user = navigation.getParam('user', 'NO-USER');
        const business = navigation.getParam('business', 'NO-USER');
        const category = navigation.getParam('category', 'null');

        const image = admin_base_url + 'images/' + category.image;
        const test_image = 'http://cybertech.multicompetition.com/images/Anchor.jpg';

        return (
            <View>
                <ScrollView style={styles.profilescroll}>
                    <View>
                        <Image style={{width: '100%', height: 200}} source={{uri: image}}/>


                        <View style={styles.row}>
                            <View style={styles.inputWrap}>
                                <Text style={styles.texts}>Product</Text>
                            </View>

                            <View style={styles.inputWrap}>
                                <Text style={styles.rightText}>{category.name}  </Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.inputWrap}>
                                <Text style={styles.texts}>Price </Text>
                            </View>

                            <View style={styles.inputWrap}>
                                <Text style={styles.rightText}> Rs. {category.price}  </Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={styles.inputWrap}>
                                <Text style={styles.texts}>Quantity </Text>
                            </View>

                            <View style={styles.inputWrap}>
                                <TextInput
                                    style={{
                                        fontSize: 20,
                                        width: '90%',
                                        marginRight: '5%',
                                        marginLeft: '25%',
                                        marginBottom: 5,
                                        marginTop: -5,
                                    }}
                                    keyboardType='numeric'
                                    onChangeText={(text) => this.onChanged(text)}
                                    value={this.state.quantity}
                                    maxLength={10}
                                />
                            </View>
                        </View>

                    </View>

                </ScrollView>


                <TouchableOpacity style={styles.button} onPress={this.addToCart}>
                    <Text style={styles.buttonText}>Add to cart </Text>
                </TouchableOpacity>
                <Modal isVisible={this.state.isModalVisible} deviceHeight={deviceHeight}
                       style={{backgroundColor: 'white', height: '50%'}}>
                    <View>
                        <Text style={styles.modalText}>How many items you want ?</Text>


                        <TouchableOpacity style={styles.button} onPress={this.addToCart}>

                            <Text style={styles.buttonText}>Add Now </Text>

                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button} onPress={this.hideQuantityArea}>

                            <Text style={styles.buttonText}>Cancel </Text>

                        </TouchableOpacity>
                    </View>

                </Modal>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        flex: 2,
        flexDirection: 'row',
    },
    inputWrap: {
        flex: 1,
        borderColor: '#cccccc',
        borderBottomWidth: 1,
        marginBottom: 5,
        marginTop: 5,
    },
    inputdate: {
        fontSize: 14,
        marginBottom: -12,
        color: '#6a4595',
    },
    inputcvv: {
        fontSize: 14,
        marginBottom: -12,
        color: '#6a4595',
    },
    profilescroll: {
        marginBottom: 0,
    },
    button: {
        width: '80%',
        backgroundColor: '#ba8cd7',
        marginRight: '10%',
        marginLeft: '10%',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 12,
    },
    modalText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#000',
        textAlign: 'center',
    },

    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
    texts: {
        fontSize: 20, width: '90%', marginRight: '5%', marginLeft: '25%', marginBottom: 5, marginTop: 5,
    },
    rightText: {
        fontSize: 20, width: '90%', marginRight: '5%', marginLeft: '5%', marginBottom: 5, marginTop: 5,
    },
    leftText: {
        fontSize: 20, width: '90%', marginRight: '5%', marginLeft: '35%', marginBottom: 5, marginTop: 5,
    },


    logoText: {
        fontSize: 25,
        fontWeight: 'bold',
        marginTop: 10,
        fontWeight: 'bold',
        color: '#ba8cd7',
    },

    headerColor: {
        backgroundColor: '#fff',
    },


});

function mapStateToProps(state) {
    return {
        cart_items: state.cart_items,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        addTodo: cart_item => dispatch(addTodo(cart_item)),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SingleProduct);


// export default SingleProduct;

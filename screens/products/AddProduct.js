import React, {Component} from 'react';
import {connect} from 'react-redux';
import {addTodo} from '../../store/reducers';
import {Text, View, TouchableOpacity, StyleSheet, AsyncStorage, Input, Form, TextInput} from 'react-native';


class ConnectedForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({[event.target.id]: event.target.value});
    }

    handleSubmit(event) {
        alert('hi');
        event.preventDefault();
        const {title} = this.state;
        this.props.addTodo({title: 'ashika', 'id': '1'});
        //this.props.emptyStore();
        //this.setState({ title: "" });
    }

    render() {
        const {title} = this.state;
        return (
            <View>
                <TextInput
                    ref={(input) => this.title = input}
                />

                <TouchableOpacity>
                    <Text onPress={this.handleSubmit}>Submit </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addTodo: cart_item => dispatch(addTodo(cart_item)),
    };
}


const AddProduct = connect(
    null,
    mapDispatchToProps,
)(ConnectedForm);

export default AddProduct;

import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, TouchableOpacity, Text, Image, Button, TextInput} from 'react-native';
import {Table, TableWrapper, Row} from 'react-native-table-component';
import {connect} from 'react-redux';
import {Container, Content, Card, CardItem, Left, Right, Body, Thumbnail, Spinner, Icon} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {deleteTodo, updateTodo, emptyStore} from '../../store/reducers';
import {CartTotal} from './CartTotal.js';
import AwesomeAlert from 'react-native-awesome-alerts';


class ExampleThree extends Component {
    constructor(props) {
        super(props);
        const {cart_total} = this.props;
        this.state = {

            tableHead: ['Product', 'Qty', 'Price'],
            tableHead1: ['Cart Total ', 'Rs.' + cart_total],
            tableHead2: ['Delivery Fee', 'Rs.25'],
            tableHead3: ['Bill Total ', 'Rs.' + (cart_total + 25)],
            showAlert: false,


            widthArr: [150, 100, 150],
            widthArr1: [250, 150],
        };
    }

    showAlert = () => {
        this.setState({
            showAlert: true,
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false,
        });
    };

    deleteItem = (cart_item) => {
        // alert(cart_item.product_id)
        this.props.deleteTodo(cart_item);


    };
    onChanged = (quantity, cart_item) => {
        // alert(quantity+" and me product_id "+ cart_item.product_id)
        this.props.updateTodo(cart_item, quantity);

    };
    RemoveAllItem = () => {
        // this.props.emptyStore()
        this.showAlert();

    };


    render() {
        const state = this.state;
        const {showAlert} = this.state;

        const {cart_items, cart_total} = this.props;
        const tableData = [];

        let c = cart_items;


        return (
            <View>


                <ScrollView horizontal={true}>

                    <View style={{marginLeft: 5, width: 350}}>


                        <ScrollView style={styles.dataWrapper}>

                            {
                                cart_items.map((rowData, index) => (
                                    <View>

                                        <Card>

                                            <CardItem>
                                                <Left style={{flex: 1}}>

                                                    <Body>


                                                        <View style={{
                                                            flex: 0.2,
                                                            borderColor: '#cccccc',
                                                            marginBottom: 5,
                                                            marginTop: 5,
                                                        }}>


                                                        </View>
                                                        <View style={styles.row}>
                                                            <View style={{
                                                                flex: 0.8,
                                                                borderColor: '#cccccc',
                                                                marginBottom: 5,
                                                                marginTop: 5,
                                                            }}>
                                                                <Text style={{
                                                                    fontWeight: 'bold',
                                                                    fontSize: 17,
                                                                }}> {rowData.product_name} </Text>
                                                            </View>
                                                            <View style={{
                                                                flex: 0.2,
                                                                borderColor: '#cccccc',
                                                                marginBottom: 5,
                                                                marginTop: 5,
                                                            }}>
                                                                <Button
                                                                    onPress={() => {
                                                                        this.deleteItem(rowData);
                                                                    }}
                                                                    title="x" color="#ff0000" style={{width: '20%'}}
                                                                />

                                                            </View>
                                                        </View>


                                                        <View style={styles.row}>
                                                            <View style={styles.inputWrap}>
                                                                <Text style={styles.texts}>Price </Text>
                                                            </View>
                                                            <View style={styles.inputWrap}>
                                                                <Text style={{
                                                                    fontWeight: 'bold',
                                                                    fontSize: 17,
                                                                }}>Rs.{rowData.price} </Text>
                                                            </View>
                                                        </View>
                                                        <View style={styles.row}>
                                                            <View style={styles.inputWrap}>
                                                                <Text style={{marginTop: 8}}>Quantity </Text>
                                                            </View>
                                                            <View style={styles.inputWrap}>
                                                                <TextInput
                                                                    style={{
                                                                        backgroundColor: '#DFE3EE',
                                                                        fontSize: 20,
                                                                        textAlign: 'center',
                                                                    }}
                                                                    keyboardType='numeric'
                                                                    onChangeText={(text) => this.onChanged(text, rowData)}
                                                                    value={rowData.quantity}
                                                                    maxLength={10}
                                                                />
                                                            </View>
                                                        </View>


                                                    </Body>
                                                </Left>

                                            </CardItem>
                                        </Card>
                                    </View>
                                ))
                            }

                            <CardItem>
                                <Left style={{flex: 1}}>

                                    <Body>


                                    </Body>
                                </Left>

                            </CardItem>

                            <CardItem cardBody>
                                <Left>
                                    <Body>
                                        <View style={styles.row}>
                                            <View style={styles.inputWrap}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Cart Total </Text>
                                            </View>
                                            <View style={styles.inputWrap}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Rs.{cart_total} </Text>
                                            </View>
                                        </View>
                                        <View style={styles.row}>
                                            <View style={styles.inputWrap}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Delivery Fee </Text>
                                            </View>
                                            <View style={styles.inputWrap}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Rs.25 </Text>
                                            </View>
                                        </View>
                                        <View style={styles.row}>
                                            <View style={styles.inputWrap}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}> Total </Text>
                                            </View>
                                            <View style={styles.inputWrap}>
                                                <Text style={{
                                                    fontWeight: 'bold',
                                                    fontSize: 17,
                                                }}>Rs.{cart_total + 25} </Text>
                                            </View>

                                        </View>
                                        <TouchableOpacity style={styles.clearBtn} onPress={this.RemoveAllItem}>
                                            <Text style={styles.buttonText}>Clear Cart </Text>
                                        </TouchableOpacity>
                                        <AwesomeAlert
                                            show={showAlert}
                                            showProgress={false}
                                            title="Are you sure? "
                                            message="Do You want to clear all ?"
                                            closeOnTouchOutside={true}
                                            closeOnHardwareBackPress={false}
                                            showCancelButton={true}
                                            showConfirmButton={true}
                                            cancelText="No, cancel"
                                            confirmText="Yes, clear it"
                                            confirmButtonColor="#DD6B55"
                                            onCancelPressed={() => {
                                                this.hideAlert();
                                            }}
                                            onConfirmPressed={() => {
                                                this.props.emptyStore();
                                            }}
                                        />
                                    </Body>

                                </Left>

                            </CardItem>
                            <CardItem cardBody>
                                <Text style={{marginBottom: 350}}> </Text>

                            </CardItem>


                        </ScrollView>


                    </View>
                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff'},
    header: {height: 50, backgroundColor: '#ba8cd7'},
    text: {textAlign: 'center', fontWeight: '100'},
    dataWrapper: {marginTop: -1},
    row: {
        flex: 2,
        flexDirection: 'row',
    },
    inputWrap: {
        flex: 1,
        borderColor: '#cccccc',
        marginBottom: 5,
        marginTop: 5,
    },
    inputWrap1: {
        flex: 0.8,
        borderColor: '#cccccc',
        borderBottomWidth: 1,
        marginBottom: 5,
        marginTop: 5,
    },
    clearBtn: {
        alignItems: 'center',
        backgroundColor: '#9B870C',
        color: '#ffffff',
        padding: 12,
        width: '80%',
        marginTop: 12,
        marginRight: '10%',
        marginLeft: '10%',
        marginBottom: 10,
        borderRadius: 25,

    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
    },
});

const mapStateToProps = state => {
    return {
        cart_items: state.cart_items,
        cart_total: state.cart_total,

    };
};

function mapDispatchToProps(dispatch) {
    return {
        deleteTodo: cart_item => dispatch(deleteTodo(cart_item)),
        updateTodo: (cart_item, quantity) => dispatch(updateTodo(cart_item, quantity)),
        emptyStore: cart_item => dispatch(emptyStore(cart_item)),

    };
}


export default ExampleThree = connect(mapStateToProps, mapDispatchToProps)(ExampleThree);



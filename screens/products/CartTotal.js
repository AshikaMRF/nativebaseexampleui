import React, {Component} from 'react';
import {StyleSheet, View, ScrollView, TouchableOpacity, Text, Image, Button, TextInput} from 'react-native';
import {Table, TableWrapper, Row} from 'react-native-table-component';
import {connect} from 'react-redux';
import {Container, Content, Card, CardItem, Left, Right, Body, Thumbnail, Spinner, Icon} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {deleteTodo, updateTodo} from '../../store/reducers';


class Total extends Component {
    constructor(props) {
        super(props);
        const {cart_total} = this.props;
        this.state = {

            tableHead: ['Product', 'Qty', 'Price'],
            tableHead1: ['Cart Total ', 'Rs.' + cart_total],
            tableHead2: ['Delivery Fee', 'Rs.25'],
            tableHead3: ['Bill Total ', 'Rs.' + (cart_total + 25)],


            widthArr: [150, 100, 150],
            widthArr1: [250, 150],
        };
    }

    deleteItem = (cart_item) => {
        // alert(cart_item.product_id)
        this.props.deleteTodo(cart_item);


    };
    onChanged = (quantity, cart_item) => {
        // alert(quantity+" and me product_id "+ cart_item.product_id)
        this.props.updateTodo(cart_item, quantity);

    };


    render() {
        const state = this.state;

        const {cart_items} = this.props;


        return (
            <View>
                <Card>
                    <CardItem cardBody>
                        <Right style={{flex: 0.8}}>
                            <Text> dggf </Text>
                        </Right>

                    </CardItem>

                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {flex: 1, padding: 16, paddingTop: 30, backgroundColor: '#fff'},
    header: {height: 50, backgroundColor: '#ba8cd7'},
    text: {textAlign: 'center', fontWeight: '100'},
    dataWrapper: {marginTop: -1},
    row: {height: 40, backgroundColor: '#E7E6E1'},
});

const mapStateToProps = state => {
    return {
        cart_items: state.cart_items,
        cart_total: state.cart_total,

    };
};

function mapDispatchToProps(dispatch) {
    return {
        deleteTodo: cart_item => dispatch(deleteTodo(cart_item)),
        updateTodo: (cart_item, quantity) => dispatch(updateTodo(cart_item, quantity)),

    };
}


export default CartTotal = connect(mapStateToProps, mapDispatchToProps)(Total);



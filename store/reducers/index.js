const ADD_TODO = 'ADD_TODO';
const UPDATE_TODO = 'UPDATE_TODO';
const DELETE_TODO = 'DELETE_TODO';
const EMPTY_STORE ='EMPTY_STORE';

const USER_DATA='USER_DATA';
const GET_TOKEN ='GET_TOKEN';
const SET_TOKEN = 'SET_TOKEN';
const DELETE_TOKEN = 'DELETE_TOKEN';
const SET_ORDERS = 'SET_ORDERS';

const GET_TOTAL ='GET_TOTAL';
const UPDATE_USER = 'UPDATE_USER';
const SET_ADDRESS ='SET_ADDRESS'
const UPDATE_USER_SINGLE = 'UPDATE_USER_SINGLE'
const LOGOUT ='LOGOUT'


const initialState = { 
    cart_items: [],
    user_data : [],
    cart_total : 0,
    my_orders : [],
    address : {

    },
    token : "",
    user : {

    }
};

export default function todo_reducer(state = initialState, action) {

  switch (action.type) {


    case ADD_TODO:
        let checkExistProduct = state.cart_items.findIndex((cart_item) =>{
            return cart_item.product_id == action.cart_item.product_id;
        });
          //let total = 0 ;


        if(checkExistProduct=="-1"){
            state.cart_items = state.cart_items.concat(action.cart_item)

        }
      state.cart_total = 0;
        for (var i =  state.cart_items.length - 1; i >= 0; i--) {
            state.cart_total = parseInt(state.cart_items[i].total) + parseInt(state.cart_total)   ;
        }


        return Object.assign({}, state, {
             cart_items: state.cart_items,
             cart_total : state.cart_total   })



        case EMPTY_STORE:
    return {
            ...state,
            cart_items: [],
            cart_total : 0 
        }

       case LOGOUT:
    return {
            ...state,
            token: ""
        }

        case SET_ORDERS:
           state.my_orders = action.my_orders
    return {
            ...state,
            my_orders:state.my_orders
        }

        case DELETE_TOKEN:
    return {
            ...state,
            user_data: [],
        }



    case UPDATE_TODO:    
        let cart_items = [...state.cart_items];
        let indexOfUpdate = cart_items.findIndex((cart_item) =>{
            return cart_item == action.cart_item;
        });  
       // alert(indexOfUpdate + " action quantity  " + action.quantity)

        cart_items[indexOfUpdate].quantity = action.quantity;
        cart_items[indexOfUpdate].total = action.quantity* cart_items[indexOfUpdate].price;
        let total1 = 0;
              cart_items: state.cart_items.filter(function(cart_item) {
                total1 =total1 +cart_item.total;
            })



        return {
            ...state,
            cart_items: cart_items,cart_total : total1 
        }


    case DELETE_TODO:
     let cart_items1 =state.cart_items.filter(cart_item => cart_item !== action.cart_item) ;
      let total2 = 0;
            cart_items1.filter(function(cart_item) {
                total2 =total2 +cart_item.total;
            })



       return { ...state,cart_items: cart_items1 , cart_total : total2  }


    case GET_TOTAL :
    let total = 0;
        // return {
            cart_items: state.cart_items.filter(function(cart_item) {
                total =total +cart_item.total;
            })
            return {
            ...state,
            cart_total: total,
        } 
        //}


    case SET_TOKEN:
             return Object.assign({}, state, {
          //   user_data:  state.user_data.concat(action.user_data)
          token : action.token 
         //cart_items: []
    });

        case UPDATE_USER:
         state.user_data[0].user = action.user_data

         console.log(state.user_data[0].user)

             return {
            ...state,
            user_data: state.user_data ,
        }

        case UPDATE_USER_SINGLE:
         state.user = action.user

       

             return {
            ...state,
            user: state.user,
        }

         case SET_ADDRESS:

         state.address = action.address

        // console.log(state.user_data[0].user)

             return {
            ...state,
            address: state.address ,
        }



       // }
        case GET_TOKEN:
    return {
            ...state,
            user_data: state.user_data ,
        }
        


    default:
      return state;
  }
}


export function addTodo(cart_item) {    
    return {
        type: ADD_TODO,
        cart_item,
    };
}
export function setAddress(address) {    
    return {
        type: SET_ADDRESS,
        address,
    };
}

export function setOrders(my_orders) {    
    return {
        type: SET_ORDERS,
        my_orders,
    };
}

export function updateTodo(cart_item,quantity){
    return {
        type: UPDATE_TODO,        
        cart_item,quantity
    }
}

export function deleteTodo(cart_item){
    return {
        type: DELETE_TODO,
        cart_item,        
    }
}

export function emptyStore(cart_item){
    return {
        type: EMPTY_STORE,
        cart_item     
    }
}
export function showCartItems(cart_item){
    return {
        type: EMPTY_STORE,
        cart_item     
    }
}

export function setToken(token){

    return {
        type: SET_TOKEN,
        token     
    }
}

export function updateUser(user_data){
   
    return {
        type: UPDATE_USER,
        user_data     
    }
}
export function updateUserSingle(user){
   
    return {
        type: UPDATE_USER_SINGLE,
        user    
    }
}

export function logout(){
   
    return {
        type: LOGOUT
         
    }
}

export function getToken(user_data){
    return {
        type: GET_TOKEN ,
        user_data    
    }
}

export function deleteToken(user_data){
    return {
        type: LOGOUT ,
        user_data    
    }
}

export function getTotal(cart_item){
    return {
        type: GET_TOTAL, cart_item   
    }
}

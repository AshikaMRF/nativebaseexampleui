import React, { Component } from "react";
import { ImageBackground, View, StatusBar } from "react-native";
import { Container, Button, H3, Text } from "native-base";

import styles from "./styles";

//const launchscreenBg = require("../../../assets/launchscreen-bg.png");
const launchscreenBg = require("../../../images/GreenBg.jpg");

const launchscreenLogo = require("../../../assets/logo-kitchen-sink.png");

class Home extends Component {
  render() {
    return (
      <Container>
        <StatusBar barStyle="light-content" />
        <ImageBackground source={launchscreenBg} style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <ImageBackground source={launchscreenLogo} style={styles.logo} />
          </View>
          <View
            style={{
              alignItems: "center",
              marginBottom: 50,
              backgroundColor: "transparent"
            }}
          >
            <H3 style={styles.text}>Rental App </H3>
            <View style={{ marginTop: 8 }} />
            <H3 style={styles.text}>Book Here </H3>
            <View style={{ marginTop: 8 }} />
          </View>
          <View style={{ marginBottom: 80 }}>
            <Button block primary
              style={{ backgroundColor: "#4179ce", alignSelf: "center", width: 200 }}
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Text>Login</Text>
            </Button>
          </View>
        </ImageBackground>
      </Container>
    );
  }
}

export default Home;
